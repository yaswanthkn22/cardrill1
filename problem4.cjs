// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot.
// Execute a function that will return an array from the dealer data containing 
//only the car years and log the result in the console as it was returned.

function get_all_years(inventory){
    if(Array.isArray(inventory) && Number.isInteger(id)){
        let years = [];
        for (let car of inventory){
            years.push(car.car_year);
        }
        return years;
    }else return [];
}

module.exports =  get_all_years