const inventory = require("../data.cjs");
const get_last_car = require("../problem2.cjs");

const result2 = get_last_car(inventory);
console.log(`Last car is a ${result2.car_make} ${result2.car_model}`)