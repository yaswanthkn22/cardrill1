const get_all_years = require("../problem4.cjs");
const get_numbercars_before_2000 = require("../problem5.cjs");
const inventory = require("../data.cjs");

const result5 = get_numbercars_before_2000(get_all_years(inventory));

console.log(result5);