
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory. 
// Execute a function and return an array that only contains BMW and Audi cars. 
// Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


function get_bmw_audi_cars (inventory){
    let bmw_audi_cars = [];
    if(Array.isArray(inventory)){
        for (let index = 0 ; index < inventory.length ; index ++){
            if (inventory[index].car_make.toUpperCase() == 'AUDI' || inventory[index].car_make.toUpperCase() == 'BMW'){
                bmw_audi_cars.push(inventory[index]);
            }
        }
    }
    return JSON.stringify(bmw_audi_cars)
}

module.exports =  get_bmw_audi_cars;