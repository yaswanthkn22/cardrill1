
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
// Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function comapre_carmodel(car1 , car2){
    const model1 = car1.car_model.toUpperCase();
    const model2 = car2.car_model.toUpperCase();
    if (model1 > model2){
        return 1;
    }
    else if(model1 < model2) {
        return -1;
    }
    return 0;
}
function order_by_car_models(inventory){
    if(Array.isArray(inventory)){
        return inventory.sort(comapre_carmodel);
    }else return [];
}

module.exports = order_by_car_models;