
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. 
//Using the array you just obtained from the previous problem, 
//find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function get_numbercars_before_2000 (result4){
    let older_cars = [];
    if(Array.isArray(result4) ){
        for (let year of result4){
            if (year < 2000){
                older_cars.push(year);
            }
        }

        console.log(older_cars.length)
}
    return older_cars;
}

module.exports =  get_numbercars_before_2000;